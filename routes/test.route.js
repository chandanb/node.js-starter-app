const testController = require('../controllers/test.controller');

module.exports = (router) => {
    router.get('/test', testController.showTestData);
}