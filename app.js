const express = require('express');
const cors = require('cors');
const app = express();
const bodyParser = require('body-parser');
const appConfig = require('./config/app.config');
const rp = require('request-promise');
var tedious = require('tedious');

var Connection = tedious.Connection;
var Request = tedious.Request;

var config = {
    userName: 'machineflaw',
    password: 'Chandan@123',
    server: 'machineflaw.database.windows.net',
    options: {
        database: 'machineflaw',
        encrypt: true,
        rowCollectionOnRequestCompletion: true
    }
}

// enable cors
app.use(cors(appConfig.corsOptions));
// support json payload requests
app.use(bodyParser.json());
// support encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

// init server
const server = require('http').createServer(app);
// pick port from env or use user defined & normalize port number
const port = normalizePort(process.env.PORT || '4000');

// set port to the app instance
app.set('port', port);

// routes for API
const router = express.Router();
// All APIs will start with / then the endpoint name
app.use('/', router);

// Register all routes here
require('./routes/test.route')(router);

/**
 * Normalize a port into a number, string, or false.
 */
function normalizePort(portNumber) {
    var port = parseInt(portNumber, 10);

    if (isNaN(port)) {
        // named pipe
        return portNumber;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

// listen to incomming requests
server.listen(port, function () {
    console.log('Server started on *: ' + port);
});

// capture server start errors
server.on('error', function(error) {
    console.log("Error in start", error); 
})