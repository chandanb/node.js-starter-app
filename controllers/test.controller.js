var showTestData = (request, response) => {
    const responseBody = {
        status: "Request successful!",
        method: "GET",
    };
    response.status(200).send(responseBody);
}

module.exports = {
    showTestData: showTestData
}