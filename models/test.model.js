module.exports = {
    showSuccessResponse: (message) => {
        return {
            status: 'success',
            message: message
        }
    }
}